# pysei


Colaboracion Pythonistica para [PSeInt](https://es.wikipedia.org/wiki/PSeInt) 

Para ver la presentación de PyCon Argentina 2019  siga [éste enlace](http://www.batlle.com.ar/PyConAr2019.html)


PSeudo Intérprete, una herramienta educativa creada en Argentina, utilizada principalmente por estudiantes para aprender los fundamentos de la programación y el desarrollo de la lógica. Es un software muy popular de su tipo y es ampliamente utilizado en universidades de Latinoamérica y España.

EL proyecto principal se lleva en [http://pseint.sourceforge.net]


---

Pythonistic collaboration for  http://pseint.sourceforge.net
